package app.jonas.emojiscratcher.ui.common

import android.graphics.BitmapFactory
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.graphics.drawscope.DrawScope
import androidx.compose.ui.platform.LocalContext
import app.jonas.emojiscratcher.R
import java.time.LocalDate
import kotlin.math.ceil
import kotlin.random.Random

@Composable
fun rememberDailyBackground(): Pair<ImageBitmap, Color> {
    val today = LocalDate.now().dayOfMonth
    val context = LocalContext.current
    val backgroundImage = remember(today, context) {
        BitmapFactory.decodeResource(
            context.resources,
            listOf(
                R.drawable.cone,
                R.drawable.cookies,
                R.drawable.spices,
                R.drawable.tree_white_bg,
                R.drawable.tree_yellow_bg,
            ).random(Random(today))
        ).asImageBitmap()
    }
    val backgroundOverlayColor = MaterialTheme.colorScheme.background.copy(alpha = 0.6f)
    return backgroundImage to backgroundOverlayColor
}

fun DrawScope.drawBackground(background: Pair<ImageBitmap, Color>) {
    val (backgroundImage, backgroundOverlayColor) = background
    for (x in 0..(ceil(size.height / backgroundImage.height).toInt())) {
        for (y in 0..(ceil(size.height / backgroundImage.height).toInt())) {
            drawImage(
                backgroundImage,
                Offset(
                    x.toFloat() * backgroundImage.width,
                    y.toFloat() * backgroundImage.height
                )
            )
        }
    }
    drawRect(backgroundOverlayColor)
}