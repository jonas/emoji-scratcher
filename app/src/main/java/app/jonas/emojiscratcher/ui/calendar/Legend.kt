package app.jonas.emojiscratcher.ui.calendar

import androidx.compose.foundation.layout.*
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Shadow
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import app.jonas.emojiscratcher.ui.theme.EmojiScratcherTheme

@Composable
fun Legend(emojis: Map<String, Int>, modifier: Modifier = Modifier) {
    val emojiList = emojis.toList()
    Column(modifier = modifier, Arrangement.Bottom) {
        emojiList.map { it.toLegendEntry() }.chunked(3).map { row ->
            Row {
                row.map { text ->
                    Box(
                        modifier = Modifier
                            .padding(8.dp)
                            .weight(1f)
                    ) {
                        Text(
                            text = text,
                            style = TextStyle(
                                color =  MaterialTheme.colorScheme.onBackground,
                                shadow = Shadow(MaterialTheme.colorScheme.inverseOnSurface, Offset(3f, 3f)),
                                fontSize = 24.sp
                            ),
                            modifier = Modifier.align(Alignment.Center)
                        )
                    }
                }
            }
        }
    }
}

fun Pair<String, Int>.toLegendEntry() = "$second ⨯ $first"

@Preview(showBackground = true)
@Composable
fun LegendPreview() {
    EmojiScratcherTheme {
        Legend(emojis = emojis)
    }
}
