package app.jonas.emojiscratcher.ui.calendar

import androidx.compose.runtime.mutableStateListOf
import androidx.compose.ui.geometry.Offset
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import app.jonas.emojiscratcher.data.scratch.ScratchDatabase
import app.jonas.emojiscratcher.data.scratch.Ticket
import app.jonas.emojiscratcher.data.toPathList
import app.jonas.emojiscratcher.data.toPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import java.time.DayOfWeek
import java.time.LocalDate
import kotlin.random.Random

data class Date(val emoji: String, val date: LocalDate, val type: DateType = DateType.byDate(date))

enum class DateType(val neededNeighbors: Int) {
    Advent(1), Normal(2), Christmas(2);

    companion object {
        fun byDate(date: LocalDate) = when {
            date.dayOfMonth == 24 -> Christmas
            date.dayOfWeek == DayOfWeek.SUNDAY -> Advent
            else -> Normal
        }
    }
}

data class CalendarUiState(
    val ticketId: Int = 0,
    val calendarRows: List<List<Date>>,
    val scratchPathList: List<List<Offset>>,
    val uiUpdater: Int = 0,
)

open class CalendarViewModel: ViewModel() {

    private var random = Random(23)

    protected var scratchPathList: MutableList<MutableList<Offset>> = mutableListOf()
    private val _uiState = MutableStateFlow(
        CalendarUiState(
            calendarRows = generateCalendar(),
            scratchPathList = scratchPathList
        )
    )
    val uiState: StateFlow<CalendarUiState> = _uiState.asStateFlow()

    open fun setTicketId(ticketId: Int) {
        _uiState.value =
            uiState.value.copy(ticketId = ticketId, calendarRows = generateCalendar(ticketId))
    }

    private fun generateCalendar(ticketId: Int = 0): List<List<Date>> {
        val random = Random(ticketId)
        val allEmojis = emojis.map { emoji -> MutableList(emoji.value) { emoji.key } }
        val winning = allEmojis.random(random)
        val emojiList = buildList {
            addAll(winning.drop(1))
            val rest = allEmojis.filter { it != winning }
            do {
                val randomList = rest.random(random)
                if (randomList.size > 1) add(randomList.removeLast())
            } while (size < 23)
        }.shuffled(random)

        val today = LocalDate.now()
        val days = (1..24).map {
            val emoji = if (it == 24) winning[0] else emojiList[it - 1] // reveal win on the 24th
            Date(emoji, LocalDate.of(today.year, 12, it))
        }
        val grouped = days.shuffled(random).groupBy { it.type }
        val normal = grouped[DateType.Normal]!!.toMutableList()

        fun Date.toRow() = buildList {
            add(this@toRow)
            repeat(type.neededNeighbors) {
                add(normal.removeLast())
            }
        }

        val advent = grouped[DateType.Advent]
        requireNotNull(advent) { "No advent days (sundays) in the given timeframe." }
        val adventDaysToDisplayAsNormal = if (advent.size > 3) advent.size - 3 else 0
        val adventRows = advent.drop(adventDaysToDisplayAsNormal).map { it.toRow() }
        val christmasRows = grouped[DateType.Christmas]?.map { it.toRow() }

        requireNotNull(christmasRows) { "No christmas in the given timeframe." }

        normal += advent.take(adventDaysToDisplayAsNormal).map { it.copy(type = DateType.Normal) }
        val normalRows = normal.chunked(3)

        return listOf(
            normalRows[0],
            adventRows[0],
            normalRows[1],
            christmasRows[0],
            normalRows[2],
            adventRows[1],
            normalRows[3],
            adventRows[2].reversed(),
        ) + normalRows.subList(4, normalRows.size)
    }

    private fun refreshUi() {
        _uiState.value = uiState.value.copy(uiUpdater = random.nextInt())
    }

    open fun insertNewScratchPath(newPoint: Offset) {
        scratchPathList.add(mutableStateListOf(newPoint))
        refreshUi()
    }

    open fun updateLatestScratchPath(newPoint: Offset) {
        val index = scratchPathList.lastIndex
        scratchPathList[index].add(newPoint)
        refreshUi()
    }
}

class PersistingCalendarViewModel(private val scratchDb: ScratchDatabase) : CalendarViewModel() {

    init {
        viewModelScope.launch(Dispatchers.IO) {
            scratchPathList += scratchDb.pointDao().loadAll().toPathList()
            val tickets = scratchDb.ticketDao().loadAll()
            if (tickets.isEmpty()) {
                scratchDb.ticketDao().insert(Ticket(ticketId = 0))
            } else {
                super.setTicketId(scratchDb.ticketDao().loadAll().last().ticketId)
            }
        }
    }

    override fun setTicketId(ticketId: Int) {
        super.setTicketId(ticketId)
        viewModelScope.launch(Dispatchers.IO) {
            scratchDb.ticketDao().update(Ticket(ticketId = ticketId))
        }
    }

    override fun insertNewScratchPath(newPoint: Offset) {
        super.insertNewScratchPath(newPoint)
        viewModelScope.launch(Dispatchers.IO) {
            scratchDb.pointDao().insertAll(newPoint.toPoint(scratchPathList.lastIndex))
        }
    }

    override fun updateLatestScratchPath(newPoint: Offset) {
        super.updateLatestScratchPath(newPoint)
        viewModelScope.launch(Dispatchers.IO) {
            scratchDb.pointDao().insertAll(newPoint.toPoint(scratchPathList.lastIndex))
        }
    }
}

class PersistingCalendarViewModelFactory(private val scratchDb: ScratchDatabase) :
    ViewModelProvider.NewInstanceFactory() {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T =
        PersistingCalendarViewModel(scratchDb) as T
}
