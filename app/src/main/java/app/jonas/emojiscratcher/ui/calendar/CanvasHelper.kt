package app.jonas.emojiscratcher.ui.calendar

import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Path

private fun calculateMidpoint(start: Offset, end: Offset) =
    Offset((start.x + end.x) / 2, (start.y + end.y) / 2)

fun List<Offset>.toPath() = Path().apply {
    if (size > 1) {
        var oldPoint: Offset? = null
        this.moveTo(get(0).x, get(0).y)
        for (i in 1 until size) {
            val point = get(i)
            oldPoint?.let {
                val midPoint = calculateMidpoint(it, point)
                if (i == 1) {
                    this.lineTo(midPoint.x, midPoint.y)
                } else {
                    this.quadraticBezierTo(it.x, it.y, midPoint.x, midPoint.y)
                }
            }
            oldPoint = point
        }
        oldPoint?.let { this.lineTo(it.x, oldPoint.y) }
    }
}