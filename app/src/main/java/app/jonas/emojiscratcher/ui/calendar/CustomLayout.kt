package app.jonas.emojiscratcher.ui.calendar

import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.LayoutCoordinates
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import app.jonas.emojiscratcher.ui.theme.EmojiScratcherTheme
import java.time.LocalDate

@Composable
fun CustomLayout(
    calendarRows: List<List<Date>>,
    modifier: Modifier = Modifier,
    onDateClick: ((LayoutCoordinates) -> Unit)? = null,
    renderDate: @Composable (BoxScope.(Date) -> Unit),
) {
    Column(modifier = modifier.padding(8.dp)) {
        calendarRows.map { datesInRow ->
            Row(modifier = Modifier.height(IntrinsicSize.Min)) {
                if (datesInRow.first().type == DateType.Christmas) {
                    Column(modifier = Modifier.weight(1f)) {
                        datesInRow.drop(1).map { date ->
                            Date(
                                date = date,
                                modifier = Modifier
                                    .weight(1f)
                                    .aspectRatio(1f),
                                onClick = onDateClick,
                                render = renderDate
                            )
                        }
                    }
                    Date(
                        date = datesInRow[0],
                        modifier = Modifier
                            .weight(2f)
                            .aspectRatio(1f),
                        onClick = onDateClick,
                        render = renderDate
                    )
                } else datesInRow.map { date ->
                    Date(
                        date = date,
                        modifier = Modifier
                            .weight(if (date.type == DateType.Advent) 2f else 1f)
                            .aspectRatio(if (date.type == DateType.Advent) 2f else 1f),
                        onClick = onDateClick,
                        render = renderDate
                    )
                }
            }
        }
    }
}

@Composable
private fun Date(
    date: Date,
    modifier: Modifier = Modifier,
    onClick: ((LayoutCoordinates) -> Unit)? = null,
    render: @Composable (BoxScope.(Date) -> Unit),
) {
    var coordinates: LayoutCoordinates? by remember {
        mutableStateOf(null)
    }
    Box(
        modifier = modifier
            .onGloballyPositioned {
                coordinates = it
            }
            .padding(8.dp)
            .border(2.dp, MaterialTheme.colorScheme.secondary)
            .clickable(enabled = onClick != null) {
                coordinates?.let { if (onClick != null) onClick(it) }
            }
            .padding(8.dp)
    ) {
        render(date)
    }
}

@Preview(showBackground = true)
@Composable
fun NormalRowPreview() {
    val calendarRows = listOf(List(3) {
        Date("", LocalDate.of(2022, 12, 2), DateType.Normal)
    })
    EmojiScratcherTheme {
        CustomLayout(calendarRows) { date -> Text(date.type.toString()) }
    }
}

@Preview(showBackground = true)
@Composable
fun AdventRowPreview() {
    val calendarRows = listOf(listOf(
        Date("", LocalDate.of(2022, 12, 2), DateType.Normal),
        Date("", LocalDate.of(2022, 12, 11), DateType.Advent),
    ))
    EmojiScratcherTheme {
        CustomLayout(calendarRows) { date -> Text(date.type.toString()) }
    }
}

@Preview(showBackground = true)
@Composable
fun ChristmasRowPreview() {
    val calendarRows = listOf(listOf(
        Date("", LocalDate.of(2022, 12, 24), DateType.Christmas),
        Date("", LocalDate.of(2022, 12, 10), DateType.Normal),
        Date("", LocalDate.of(2022, 12, 2), DateType.Normal),
    ))
    EmojiScratcherTheme {
        CustomLayout(calendarRows) { date -> Text(date.type.toString()) }
    }
}