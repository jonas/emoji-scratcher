@file:OptIn(ExperimentalMaterial3Api::class, ExperimentalComposeUiApi::class)

package app.jonas.emojiscratcher.ui.calendar

import androidx.activity.compose.BackHandler
import androidx.compose.animation.core.SpringSpec
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.gestures.detectDragGestures
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Check
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.*
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.layout.LayoutCoordinates
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.layout.positionInRoot
import androidx.compose.ui.platform.*
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import app.jonas.emojiscratcher.R
import app.jonas.emojiscratcher.data.scratch.providesScratchDatabase
import app.jonas.emojiscratcher.ui.common.drawBackground
import app.jonas.emojiscratcher.ui.common.rememberDailyBackground
import app.jonas.emojiscratcher.ui.theme.EmojiScratcherTheme
import kotlinx.coroutines.delay

@Composable
fun CalendarRoute(
    viewModel: CalendarViewModel = viewModel(
        factory = PersistingCalendarViewModelFactory(
            providesScratchDatabase(LocalContext.current)
        )
    )
) {
    var scale by remember { mutableStateOf(1f) }
    var offset by remember { mutableStateOf(Offset(0f, 0f)) }
    var isScratchingEnabled by remember { mutableStateOf(false) }
    BackHandler(isScratchingEnabled) {
        scale = 1f
        offset = Offset(0f, 0f)
        isScratchingEnabled = false
    }
    val uiState = viewModel.uiState.collectAsState()
    CalendarScreen(
        ticketId = uiState.value.ticketId,
        setTicketId = viewModel::setTicketId,
        uiState.value.calendarRows,
        isScratchingEnabled,
        setScratchingEnabled = { isScratchingEnabled = it },
        scale,
        offset,
        setScale = { scale = it },
        setOffset = { offset = it },
        scratchPathList = uiState.value.scratchPathList,
        insertNewScratchPath = viewModel::insertNewScratchPath,
        updateLatestScratchPath = viewModel::updateLatestScratchPath,
    )
}

@Composable
fun CalendarScreen(
    ticketId: Int,
    setTicketId: (Int) -> Unit,
    calendarRows: List<List<Date>>,
    isScratchingEnabled: Boolean,
    setScratchingEnabled: (Boolean) -> Unit,
    scale: Float,
    offset: Offset,
    setScale: (Float) -> Unit,
    setOffset: (Offset) -> Unit,
    scratchPathList: List<List<Offset>>,
    insertNewScratchPath: (Offset) -> Unit,
    updateLatestScratchPath: (Offset) -> Unit,
) {
    Scaffold(
        modifier = Modifier,
        floatingActionButton = {
            if (!isScratchingEnabled) {
                ExtendedFloatingActionButton(
                    onClick = { setScratchingEnabled(true) },
                    modifier = Modifier
                ) {
                    Text(
                        text = "🪙",
                        modifier = Modifier.padding(end = 16.dp),
                        fontSize = 24.sp,
                    )
                    Text(
                        text = stringResource(id = R.string.scratch),
                        style = MaterialTheme.typography.bodyLarge,
                    )
                }
            } else {
                ExtendedFloatingActionButton(
                    onClick = {
                        setScale(1f)
                        setOffset(Offset(0f, 0f))
                        setScratchingEnabled(false)
                    },
                    modifier = Modifier
                ) {
                    Icon(
                        imageVector = Icons.Default.Check,
                        contentDescription = null,
                        modifier = Modifier
                            .padding(end = 16.dp)
                            .size(24.dp)
                    )
                    Text(
                        text = stringResource(id = R.string.finished_scratching),
                        style = MaterialTheme.typography.bodyLarge,
                    )
                }
            }
        },
        floatingActionButtonPosition = FabPosition.Center,
        containerColor = MaterialTheme.colorScheme.background
    ) { paddingValues ->
        Calendar(
            ticketId,
            setTicketId,
            calendarRows,
            isScratchingEnabled,
            setScratchingEnabled,
            scale,
            offset,
            setScale,
            setOffset,
            scratchPathList,
            insertNewScratchPath,
            updateLatestScratchPath,
            modifier = Modifier.padding(paddingValues),
        )
    }
}

data class TopPadding(val px: Int, val dp: Dp)

@Composable
fun topPadding(px: Int) = TopPadding(px, dp = with(LocalDensity.current) { px.toDp() })

@Composable
fun Calendar(
    ticketId: Int,
    setTicketId: (Int) -> Unit,
    calendarRows: List<List<Date>>,
    isScratchingEnabled: Boolean,
    setScratchingEnabled: (Boolean) -> Unit,
    scale: Float,
    offset: Offset,
    setScale: (Float) -> Unit,
    setOffset: (Offset) -> Unit,
    scratchPathList: List<List<Offset>>,
    insertNewScratchPath: (Offset) -> Unit,
    updateLatestScratchPath: (Offset) -> Unit,
    modifier: Modifier = Modifier,
) {
    val focusManager = LocalFocusManager.current
    val focusRequester = remember { FocusRequester() }
    val keyboard = LocalSoftwareKeyboardController.current
    val fabSpacerDp = 80.dp
    val fabSpacerPx = with(LocalDensity.current) { fabSpacerDp.toPx() }
    var containerCoordinates: LayoutCoordinates? by remember { mutableStateOf(null) }
    var topInfoCoordinates: LayoutCoordinates? by remember { mutableStateOf(null) }
    val animatedScale = animateFloatAsState(targetValue = scale)
    val animatedOffsetX = animateFloatAsState(targetValue = offset.x)
    val animatedOffsetY = animateFloatAsState(targetValue = offset.y)
    val scrollState = rememberScrollState()
    val topPadding = topPadding(px = topInfoCoordinates?.size?.height ?: 0)

    LaunchedEffect(ticketId) {
        delay(300) // wait for some time if there is data coming from the database
        if (ticketId == 0) {
            focusRequester.requestFocus()
            delay(100)
            keyboard?.show()
        }
    }

    LaunchedEffect(topPadding.px) {
        if (ticketId != 0) scrollState.animateScrollTo(topPadding.px, SpringSpec(stiffness = 10f))
    }

    fun onDateClick(coordinates: LayoutCoordinates) {
        focusManager.clearFocus()
        keyboard?.hide()
        val containerSize = containerCoordinates?.size
        requireNotNull(containerSize) { "Date was clicked before container size was evaluated." }
        val newScale = containerSize.width.toFloat() / coordinates.size.width
        setScale(newScale)
        coordinates.positionInRoot().let {
            // val extra = coordinates.size.height.toFloat() / 2 // shift half its size from top
            // shift to the bottom (over the FAB)
            val extra = (containerSize.height - fabSpacerPx) / newScale - coordinates.size.height
            setOffset(it.copy(y = it.y - extra) * -1f)
        }
        setScratchingEnabled(true)
    }

    Box(
        modifier = modifier
            .graphicsLayer(
                scaleX = animatedScale.value,
                scaleY = animatedScale.value,
                transformOrigin = TransformOrigin(0f, 0f),
                translationX = animatedOffsetX.value * animatedScale.value,
                translationY = animatedOffsetY.value * animatedScale.value,
            )
            .onGloballyPositioned { coordinates ->
                containerCoordinates = coordinates
            }
    ) {
        Box(modifier = Modifier.verticalScroll(scrollState, enabled = !isScratchingEnabled)) {
            if (containerCoordinates != null) CustomLayout(
                calendarRows,
                modifier = Modifier.padding(top = topPadding.dp, bottom = fabSpacerDp)
            ) { date ->
                Text(
                    text = date.emoji,
                    modifier = Modifier.align(Alignment.Center),
                    fontSize = if (date.type == DateType.Christmas) 80.sp else 40.sp,
                )
                // Text(text = "Day ${date.date.dayOfMonth}!\n${coordinates?.positionInRoot()?.y}")
            }
            val background = rememberDailyBackground()
            CustomLayout(
                calendarRows,
                modifier = Modifier
                    .then(if (isScratchingEnabled) Modifier.pointerInput(Unit) {
                        detectDragGestures(
                            onDragStart = { offset ->
                                insertNewScratchPath(offset)
                            }
                        ) { change, _ ->
                            val newPoint = change.position
                            updateLatestScratchPath(newPoint)
                        }
                    } else Modifier)
                    .graphicsLayer(
                        alpha = if (containerCoordinates != null || LocalInspectionMode.current) {
                            0.99f // This hack is needed to use BlendMode.Clear on the canvas.
                        } else 0f
                    )
                    .drawBehind {
                        drawBackground(background)
                        scratchPathList.forEach { points ->
                            drawPath(
                                points.toPath(),
                                color = Color.Transparent,
                                style = Stroke(
                                    width = 30f,
                                    cap = StrokeCap.Round,
                                    join = StrokeJoin.Round
                                ),
                                blendMode = BlendMode.Clear
                            )
                        }
                    }
                    .padding(top = topPadding.dp, bottom = fabSpacerDp),
                onDateClick = if (!isScratchingEnabled) ::onDateClick else null
            ) { date ->
                Text(text = "${date.date.dayOfMonth}", style = TextStyle(
                    color =  MaterialTheme.colorScheme.onBackground,
                    shadow = Shadow(MaterialTheme.colorScheme.inverseOnSurface, Offset(3f, 3f)),
                    fontSize = 24.sp
                ))
                // Text(text = "Day ${date.date.dayOfMonth}!\n${coordinates?.positionInRoot()?.y}")
            }
                Column(modifier = Modifier
                    .padding(16.dp)
                    .onGloballyPositioned { topInfoCoordinates = it }
                    .graphicsLayer(alpha = if (topInfoCoordinates != null) 1f else 0f)
                ) {
                    TextField(
                        value = if (ticketId == 0) "" else ticketId.toString(),
                        onValueChange = { text ->
                            try {
                                val number = if (text.isEmpty()) 0 else text.toInt()
                                setTicketId(number)
                            } catch (_: NumberFormatException) {
                            }
                        },
                        modifier = Modifier
                            .fillMaxWidth()
                            .focusRequester(focusRequester),
                        placeholder = {
                            Text(
                                stringResource(R.string.ticket_number),
                                fontSize = 24.sp
                            )
                        },
                        textStyle = LocalTextStyle.current.copy(fontSize = 24.sp),
                        keyboardOptions = KeyboardOptions(
                            keyboardType = KeyboardType.Number,
                            autoCorrect = false,
                            imeAction = ImeAction.Done
                        ),
                        keyboardActions = KeyboardActions {
                            keyboard?.hide()
                            focusManager.clearFocus()
                        }
                    )
                    Legend(emojis, modifier = Modifier.padding(vertical = 8.dp))
                }
        }
    }
}

@Preview
@Composable
fun CalendarScreenInteractivePreview() {
    EmojiScratcherTheme {
        CalendarRoute(viewModel())
    }
}
