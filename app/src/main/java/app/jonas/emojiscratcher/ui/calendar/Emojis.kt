package app.jonas.emojiscratcher.ui.calendar

val emojis = mapOf(
    "🌟" to 11, // ⭐
    "🎁" to 10,
    "🎅" to 9, // 🧑‍🎄
    "🕯️" to 8,
    "🎄" to 7,
    "☃️" to 6, // ⛄
    "❄️" to 5,
    "🦌" to 4, // 🍬
    "🔔" to 3,
)