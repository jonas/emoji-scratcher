package app.jonas.emojiscratcher

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import app.jonas.emojiscratcher.ui.calendar.CalendarRoute
import app.jonas.emojiscratcher.ui.theme.EmojiScratcherTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            EmojiScratcherTheme {
                CalendarRoute()
            }
        }
    }
}
