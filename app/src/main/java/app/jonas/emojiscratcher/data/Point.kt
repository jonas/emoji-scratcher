package app.jonas.emojiscratcher.data

import androidx.compose.ui.geometry.Offset
import app.jonas.emojiscratcher.data.scratch.Point

fun List<Point>.toPathList(): MutableList<MutableList<Offset>> =
    groupBy { it.path }
        .map { (_, list) -> list.map { Offset(it.x, it.y) }.toMutableList() }.toMutableList()

fun Offset.toPoint(path: Int) = Point(path, x, y)