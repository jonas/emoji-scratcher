package app.jonas.emojiscratcher.data.scratch

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Point::class, Ticket::class], version = 1)
abstract class ScratchDatabase : RoomDatabase() {
    abstract fun pointDao(): PointDao
    abstract fun ticketDao(): TicketDao
}

fun providesScratchDatabase(
    context: Context,
): ScratchDatabase = Room.databaseBuilder(
    context,
    ScratchDatabase::class.java,
    "scratch-database"
).build()