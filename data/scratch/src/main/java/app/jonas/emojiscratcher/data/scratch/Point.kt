package app.jonas.emojiscratcher.data.scratch

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Point(
  var path: Int,
  var x: Float,
  var y: Float,
) {
  @PrimaryKey(autoGenerate = true)
  var id: Long? = null
}