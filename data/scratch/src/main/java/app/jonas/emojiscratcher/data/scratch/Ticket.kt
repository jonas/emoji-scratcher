package app.jonas.emojiscratcher.data.scratch

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Ticket(
  @PrimaryKey
  var id: Int = 0, // currently only one ticket gets saved
  var ticketId: Int
)