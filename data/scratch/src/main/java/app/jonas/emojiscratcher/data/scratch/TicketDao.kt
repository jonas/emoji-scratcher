package app.jonas.emojiscratcher.data.scratch

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface TicketDao {
    @Query("SELECT * FROM ticket WHERE id == 0 LIMIT 1")
    fun loadAll(): List<Ticket>

    @Insert
    fun insert(ticket: Ticket)

    @Update
    fun update(ticket: Ticket)
}