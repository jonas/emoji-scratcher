package app.jonas.emojiscratcher.data.scratch

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface PointDao {
    @Query("SELECT * FROM point")
    fun loadAll(): List<Point>

    @Insert
    fun insertAll(vararg points: Point)
}